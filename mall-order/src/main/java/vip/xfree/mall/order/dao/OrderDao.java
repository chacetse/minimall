package vip.xfree.mall.order.dao;

import vip.xfree.mall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author Chace Tse
 * @email chencai.xie@gmail.com
 * @date 2020-04-08 22:16:49
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
