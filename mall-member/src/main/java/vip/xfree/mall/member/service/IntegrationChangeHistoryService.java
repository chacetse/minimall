package vip.xfree.mall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.xfree.common.utils.PageUtils;
import vip.xfree.mall.member.entity.IntegrationChangeHistoryEntity;

import java.util.Map;

/**
 * 积分变化历史记录
 *
 * @author Chace Tse
 * @email chencai.xie@gmail.com
 * @date 2020-04-08 22:20:41
 */
public interface IntegrationChangeHistoryService extends IService<IntegrationChangeHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

