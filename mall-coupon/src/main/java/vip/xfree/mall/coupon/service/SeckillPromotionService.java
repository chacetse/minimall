package vip.xfree.mall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.xfree.common.utils.PageUtils;
import vip.xfree.mall.coupon.entity.SeckillPromotionEntity;

import java.util.Map;

/**
 * 秒杀活动
 *
 * @author Chace Tse
 * @email chencai.xie@gmail.com
 * @date 2020-04-08 22:11:36
 */
public interface SeckillPromotionService extends IService<SeckillPromotionEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

