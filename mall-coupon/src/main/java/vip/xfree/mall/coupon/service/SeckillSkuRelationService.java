package vip.xfree.mall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.xfree.common.utils.PageUtils;
import vip.xfree.mall.coupon.entity.SeckillSkuRelationEntity;

import java.util.Map;

/**
 * 秒杀活动商品关联
 *
 * @author Chace Tse
 * @email chencai.xie@gmail.com
 * @date 2020-04-08 22:11:36
 */
public interface SeckillSkuRelationService extends IService<SeckillSkuRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

