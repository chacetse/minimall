package vip.xfree.mall.coupon.dao;

import vip.xfree.mall.coupon.entity.SpuBoundsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品spu积分设置
 * 
 * @author Chace Tse
 * @email chencai.xie@gmail.com
 * @date 2020-04-08 22:11:36
 */
@Mapper
public interface SpuBoundsDao extends BaseMapper<SpuBoundsEntity> {
	
}
