package vip.xfree.mall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.xfree.common.utils.PageUtils;
import vip.xfree.mall.ware.entity.PurchaseEntity;

import java.util.Map;

/**
 * 采购信息
 *
 * @author Chace Tse
 * @email chencai.xie@gmail.com
 * @date 2020-04-08 22:23:41
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

