package vip.xfree.mall.ware.dao;

import vip.xfree.mall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author Chace Tse
 * @email chencai.xie@gmail.com
 * @date 2020-04-08 22:23:41
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
