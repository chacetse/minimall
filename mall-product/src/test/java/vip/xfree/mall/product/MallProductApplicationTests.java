package vip.xfree.mall.product;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import vip.xfree.mall.product.entity.BrandEntity;
import vip.xfree.mall.product.service.BrandService;

@SpringBootTest
@RunWith(SpringRunner.class)
class MallProductApplicationTests {
    @Autowired
    BrandService brandService;


    @Test
    void contextLoads() {
//        BrandEntity brandEntity = new BrandEntity();
//        brandEntity.setName("小米");
//        brandService.save(brandEntity);

        BrandEntity byId = brandService.getById(3);
        byId.setLogo("小米");
        brandService.update();
        System.out.println("保存成功！");
    }

}
