package vip.xfree.mall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.xfree.common.utils.PageUtils;
import vip.xfree.mall.product.entity.AttrEntity;

import java.util.Map;

/**
 * 商品属性
 *
 * @author Chace Tse
 * @email chencai.xie@gmail.com
 * @date 2020-04-08 18:21:46
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

