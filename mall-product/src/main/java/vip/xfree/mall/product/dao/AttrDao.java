package vip.xfree.mall.product.dao;

import vip.xfree.mall.product.entity.AttrEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品属性
 * 
 * @author Chace Tse
 * @email chencai.xie@gmail.com
 * @date 2020-04-08 18:21:46
 */
@Mapper
public interface AttrDao extends BaseMapper<AttrEntity> {
	
}
