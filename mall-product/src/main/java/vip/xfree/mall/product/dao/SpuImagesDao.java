package vip.xfree.mall.product.dao;

import vip.xfree.mall.product.entity.SpuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu图片
 * 
 * @author Chace Tse
 * @email chencai.xie@gmail.com
 * @date 2020-04-08 18:21:45
 */
@Mapper
public interface SpuImagesDao extends BaseMapper<SpuImagesEntity> {
	
}
